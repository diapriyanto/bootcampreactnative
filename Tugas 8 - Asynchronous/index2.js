let readBooksPromise = require("./promise.js");

let books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

//Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[0]).then((sisaWaktu) =>
  readBooksPromise(sisaWaktu, books[1]).then((sisaWaktu2) =>
    readBooksPromise(sisaWaktu2, books[2])
  )
);
