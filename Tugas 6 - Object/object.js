console.log("-------------------------------");
console.log("Soal No. 1 (Array to Object)");
console.log("-------------------------------");

function arrayToObject(arr) {
  let x = arr.length;
  if (x <= 0) {
    return console.log("");
  }
  for (let i = 0; i < x; i++) {
    thisObject = {};

    let birthYear = arr[i][3];
    let today = new Date().getFullYear();
    let thisAge;

    if (birthYear && today - birthYear > 0) {
      thisAge = today - birthYear;
    } else {
      thisAge = "Invalid Birth Year";
    }
    thisObject.firstName = arr[i][0];
    thisObject.lastName = arr[i][1];
    thisObject.gender = arr[i][2];
    thisObject.age = thisAge;

    let consolethisObject =
      i + 1 + " " + thisObject.firstName + " " + thisObject.lastName + " :";
    console.log(consolethisObject);
    console.log(thisObject);
  }
}

let people = [
  ["Bruce", "Banner", "Male", 1975],
  ["Natasha", "Romanoff", "female"],
]; // 1. Christ Evans:
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
console.log(" ");
let people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case

arrayToObject([]);

console.log("-------------------------------");
console.log("Soal No. 2 (Shooping Time)");
console.log("-------------------------------");

function shoppingTime(memberId, money) {
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    let nowObject = {};
    let changeMoney = money;
    let listPurchased = [];

    let sepatu = "Sepatu Brand Stacattu";
    let Zoro = "Baju Brand Zoro";
    let HdanN = "Baju Brand H&N";
    let sweater = "Sweater brand Uniklooh";
    let casingHP = "Casing Handphone";

    let test = 0;

    for (let i = 0; changeMoney >= 50000 && test == 0; i++) {
      if (changeMoney >= 1500000) {
        listPurchased.push = sepatu;
        changeMoney -= 1500000;
      } else if (changeMoney >= 500000) {
        listPurchased.push = Zoro;
        changeMoney -= 500000;
      } else if (changeMoney >= 250000) {
        listPurchased.push = HdanN;
        changeMoney -= 250000;
      } else if (changeMoney >= 175000) {
        listPurchased.push = sweater;
        changeMoney -= 175000;
      } else if (changeMoney >= 50000) {
        if (listPurchased.length != 0) {
          for (let j = 0; j <= listPurchased.length - 1; j++) {
            if (listPurchased[j] == casingHP) {
              test += 1;
            }
          }
          if ((test = 0)) {
            listPurchased.push = casingHP;
            changeMoney -= 50000;
          }
        } else {
          listPurchased.push(casingHP);
          changeMoney -= 50000;
        }
      }
      nowObject.memberId = memberId;
      nowObject.money = money;
      nowObject.listPurchased = listPurchased;
      nowObject.changeMoney = changeMoney;

      return nowObject;
    }
  }
}
// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("-------------------------------");
console.log("Soal No. 3 (Naik Angkot)");
console.log("-------------------------------");

function naikAngkot(penumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  arrOfouput = [];

  if (penumpang.length <= 0) {
    return [];
  }
  for (let i = 0; i < penumpang.length; i++) {
    let objOfoutput = {};
    let asal = penumpang[i][1];
    let tujuan = penumpang[i][2];

    let indexAsal;
    let indexTujuan;

    for (let j = 0; j < rute.length; j++) {
      if (rute[j] == asal) {
        indexAsal = j;
      } else if (rute[j] == tujuan) {
        indexTujuan = j;
      }
    }
    let bayar = (indexTujuan - indexAsal) * 2000;

    objOfoutput.pelanggan = penumpang[i][0];
    objOfoutput.asal = asal;
    objOfoutput.tujuan = tujuan;
    objOfoutput.bayar = bayar;

    arrOfouput.push(objOfoutput);
  }
  return arrOfouput;
}
//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([]));
