//==============================

console.log("Soal No .1 (Range)");
console.log("--------------------------------");
function range(startNum, finishNum) {
  let result = [];
  if (startNum == null && finishNum == null) {
    return -1;
  } else if (
    (startNum == null && finishNum >= 0) ||
    (startNum >= 0 && finishNum == null)
  ) {
    return -1;
  } else {
    if (startNum > finishNum) {
      for (let i = startNum; i >= finishNum; i--) {
        result.push(i);
      }
    } else {
      for (let i = startNum; i <= finishNum; i++) {
        result.push(i);
      }
    }
  }
  return result;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1
console.log("--------------------------------");

console.log("Soal No .2 (Range With Step)");
console.log("--------------------------------");

function rangeWithStep(startNum, finishNum, step) {
  let result = [];
  if (startNum == null && finishNum == null) {
    return -1;
  } else if (
    (startNum == null && finishNum >= 0) ||
    (startNum >= 0 && finishNum == null)
  ) {
    return -1;
  } else {
    if (startNum > finishNum) {
      for (let i = startNum; i >= finishNum; i -= step) {
        result.push(i);
      }
    } else {
      for (let i = startNum; i <= finishNum; i += step) {
        result.push(i);
      }
    }
  }
  return result;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
console.log("--------------------------------");

console.log("Soal No .3 (Sum of Range)");
console.log("--------------------------------");

function sum(startNum, finishNum, step) {
  let output = 0;
  if (startNum == null && finishNum == null) {
    return 0;
  } else if (startNum == null) {
    return finishNum;
  } else if (finishNum == null) {
    return startNum;
  } else {
    if (step == null) {
      step = 1;
    }
    if (startNum > finishNum) {
      for (let i = startNum; i >= finishNum; i -= step) {
        output += i;
      }
    } else {
      for (let i = startNum; i <= finishNum; i += step) {
        output += i;
      }
    }
  }
  return output;
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log("--------------------------------");

console.log("Soal No .4 (Array Multidimensi)");
console.log("--------------------------------");

let data = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(file) {
  for (var i = 0; i < file.length; i++) {
    console.log("Nomor ID: " + file[i][0]);
    console.log("Nama Lengkap: " + file[i][1]);
    console.log("TTL: " + file[i][2] + " " + file[i][3]);
    console.log("Hobi: " + file[i][4] + "\n");
  }
}

dataHandling(data);
console.log("--------------------------------");

console.log("Soal No .5 (Balik Kata)");
console.log("--------------------------------");

function balikKata(kata) {
  return kata.split("").reverse().join("");
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log("--------------------------------");

console.log("Soal No .6 (Metode Array)");
console.log("--------------------------------");
var input = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
dataHandling2(input);
function dataHandling2(input) {
  input.splice(1, 1, input[1] + "Elsharawy");
  input.splice(2, 1, "Provinsi " + input[2]);
  input.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(input);

  var ttl = String(input.slice(3, 4));
  var Bulan = ttl.split("/");
  switch (Bulan[1]) {
    case "01":
      var namaBulan = "Januari";
      break;
    case "02":
      var namaBulan = "Februari";
      break;
    case "03":
      var namaBulan = "Maret";
      break;
    case "04":
      var namaBulan = "April";
      break;
    case "05":
      var namaBulan = "Mei";
      break;
    case "06":
      var namaBulan = "Juni";
      break;
    case "07":
      var namaBulan = "Juli";
      break;
    case "08":
      var namaBulan = "Agustus";
      break;
    case "09":
      var namaBulan = "September";
      break;
    case "10":
      var namaBulan = "Oktober";
      break;
    case "11":
      var namaBulan = "November";
      break;
    case "12":
      var namaBulan = "Desember";
      break;
    default:
      break;
  }
  console.log(namaBulan);
  console.log(
    Bulan.sort(function (value1, value2) {
      return value2 - value1;
    })
  );
  var Bulan = ttl.split("/");
  console.log(Bulan.join("-"));
  var nama = String(input.slice(1, 2));
  console.log(nama.slice(0, 14));
}
