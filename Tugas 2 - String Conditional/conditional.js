var nama = "Dab";
var peran = "Werewolf";

if (nama.length <= 0 && peran.length <= 0) {
    console.log("Nama harus diisi!");
} else if (nama.length > 0 && peran.length <= 0) {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else if (nama.length > 0 && peran.toLowerCase() === "penyihir") {
    console.log(`Selamat datang di dunia werewolf, ${nama}\n`);
    console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
} else if (nama.length > 0 && peran.toLowerCase() === "guard") {
    console.log(`Selamat datang di dunia werewolf, ${nama}\n`);
    console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
} else if (nama.length > 0 && peran.toLowerCase() === "werewolf") {
    console.log(`Selamat datang di dunia werewolf, ${nama}\n`);
    console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`);
} else {
    console.log("Peran yang anda mainkan tidak ada");
}
console.log("---------------------------------------------------------------------------------------------");


var tanggal = 16; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 2; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2021; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if (tanggal >= 1 && tanggal <= 31 && tahun >= 1900 && tahun <= 2200) {
  switch (bulan) {
    case 1:
      console.log(`Tanggal: ${tanggal} Januari ${tahun}`);
      break;
    case 2:
      tanggal > 28
        ? console.log(`Pebruari tahun ini maksimal 28 hari`) : console.log(`Tanggal: ${tanggal} Februari ${tahun}`);
      break;
    case 3:
      console.log(`Tanggal: ${tanggal} Maret ${tahun}`);
      break;
    case 4:
      console.log(`Tanggal: ${tanggal} April ${tahun}`);
      break;
    case 5:
      console.log(`Tanggal: ${tanggal} Mei ${tahun}`);
      break;
    case 6:
      console.log(`Tanggal: ${tanggal} Juni ${tahun}`);
      break;
    case 7:
      console.log(`Tanggal: ${tanggal} Juli ${tahun}`);
      break;
    case 8:
      console.log(`Tanggal: ${tanggal} Agustus ${tahun}`);
      break;
    case 9:
      console.log(`Tanggal: ${tanggal} September ${tahun}`);
      break;
    case 10:
      console.log(`Tanggal: ${tanggal} Oktober ${tahun}`);
      break;
    case 11:
      console.log(`Tanggal: ${tanggal} November ${tahun}`);
      break;
    case 12:
      console.log(`Tanggal: ${tanggal} Desember ${tahun}`);
      break;
    default:
      break;
  }
} else {
  console.log("Tidak sesuai dengan data");
}

console.log("---------------------------------------------------------------------------------------------");
