//===================================================

console.log("====================================");
console.log("Soal 1.");
console.log("LOOPING PERTAMA");
console.log("====================================");

let genap = 2;
while (genap <= 20) {
  console.log(genap + " - I Love Coding");
  genap += 2;
}

//===================================================

console.log("====================================");
console.log("LOOPING KEDUA");
console.log("====================================");

let a = 20;
while (a > 0) {
  console.log(a + " - I will become a mobile developer");
  a -= 2;
}

//===================================================
console.log("====================================");
console.log("Soal 2.");
console.log("LOOPING FOR");
console.log("====================================");

for (let angka = 1; angka <= 20; angka++) {
  if (angka % 2 == 0) {
    console.log(angka + " - Berkualitas");
  } else if (angka % 3 == 0 && angka % 2 == 1) {
    console.log(angka + " - I Love Coding");
  } else if (angka % 2 == 1) {
    console.log(angka + " - Santai");
  }
}

//===================================================
console.log("====================================");
console.log("Soal 3.");
console.log("MEMBUAT PERSEGI PANJANG");
console.log("====================================");

let lebar = 8;
let panjang = 3;
let papan = "";

for (let d = 0; d <= lebar; d++) {
  papan += "#"; //papan = "#" 8 ke samping
}
for (let e = 0; e <= panjang; e++) {
  console.log(papan);
}

//===================================================
console.log("====================================");
console.log("Soal 4.");
console.log("MEMBUAT TANGGA");
console.log("====================================");

tangga = "";
for (let i = 0; i < 8; i++) {
  for (let a = 0; a < i; a++) {
    tangga += "#";
  }
  tangga += "\n";
}
console.log(tangga);

//===================================================//
console.log("====================================");
console.log("Soal 5.");
console.log("MEMBUAT PAPAN CATUR");
console.log("====================================");

// let o;
// let p;
// sana = 7;
// sini = 7;
// catur = " ";

// for (o = 0; o <= sana; o++) {
//   if (o % 2 == 1) {
//     for (p = 0; p <= sini; p++) {
//       if (p % 2 == 1) {
//         catur += " ";
//       } else {
//         catur += "#";
//       }
//     }
//   } else {
//     for (p = 0; p <= sini; p++) {
//       if (p % 2 == 1) {
//         catur += "#";
//       } else {
//         catur += " ";
//       }
//     }
//   }
//   console.log(catur);
//   catur = " ";
// }

for (let i = 0; i < 8; i++) {
  if (i % 2 == 0) {
    for (let a = 0; a < 4; a++) {
      process.stdout.write(" #");
    }
    console.log("");
  } else {
    for (let a = 0; a < 4; a++) {
      process.stdout.write("# ");
    }
    console.log("");
  }
}

let apalah = 10;
let apalah2 = 0; //1
for (let i = 0; i < apalah; i++) {
  if (i % 2 != 0) {
    for (let b = 0; b < apalah / 2 - apalah2; b++) {
      process.stdout.write(" ");
    }

    for (let a = 0; a < i; a++) {
      process.stdout.write("#");
    }
    apalah2 += 1;
    process.stdout.write("\n");
  }
}
