//==========Soal 1. Animal Class===========
console.log("============================");
console.log("Soal No.1 Animal Class");
console.log("============================");

console.log("> Release 1");
class Animal {
  constructor(name, legs = 4, cold_blooded = false) {
    this._name = name;
    this._legs = legs;
    this._cold_blooded = cold_blooded;
  }

  get name() {
    return this._name;
  }

  get legs() {
    return this._legs;
  }

  get cold_blooded() {
    return this._cold_blooded;
  }

  set name(arg) {
    this._name = arg;
  }

  set legs(arg) {
    this._legs = arg;
  }

  set cold_blooded(arg) {
    this._cold_blooded = arg;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

console.log("> Release 0");
class Frog extends Animal {
  constructor(name, cold_blooded) {
    super(name, cold_blooded);
    this._legs = 4;
  }

  jump() {
    console.log("hop hop");
  }
}

class Ape extends Animal {
  constructor(name, cold_blooded) {
    super(name, cold_blooded);
    this._legs = 2;
  }

  yell() {
    console.log("Auooo");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

//==========Soal 2. Function to Class===========
console.log("============================");
console.log("Soal No.2 Function to Class");
console.log("============================");

/*
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); */

class Clock {
  // Code di sini
  constructor(object) {
    this._object = object;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this._object["template"]
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  start() {
    this.render();
    setInterval(this.render.bind(this), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
