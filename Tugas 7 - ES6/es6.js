//==========Soal No 1 Mengubah fungsi menjadi fungsi arrow==========
console.log("-------------------------------");
console.log("Soal No. 1 Mengubah fungsi menjadi fungsi arrow");
console.log("-------------------------------");

const golden1 = (goldenFunction = () => {
  console.log("this is golden!!");
});

golden1();

//==========Soal No. 2 Sederhanakan menjadi Object literal di ES6==========
console.log("-------------------------------");
console.log("Soal No. 2 Sederhanakan menjadi Object literal di ES6");
console.log("-------------------------------");

newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => console.log(`${firstName} ${lastName}`),
  };
};
newFunction("William", "Imoh").fullName();

console.log("-------------------------------");
console.log("Soal No. 3 Destructuring");
console.log("-------------------------------");
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

//Driver Code
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

console.log("-------------------------------");
console.log("Soal No. 4 Array Spreading");
console.log("-------------------------------");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combinedArray = [...west, ...east];
//Driver Code
console.log(combinedArray);

console.log("-------------------------------");
console.log("Soal No. 5 Template Literals");
console.log("-------------------------------");

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
console.log(before);
