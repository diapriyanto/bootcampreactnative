//=======================================================
console.log("Soal No. 1");
console.log("------------------------------------------");
function teriak() {
  return "Hallo Sanbers!";
}

console.log(teriak());

console.log("------------------------------------------");

//=======================================================
console.log("Soal No. 2");
console.log("------------------------------------------");

function kalikan(num1, num2) {
  return num1 * num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log("------------------------------------------");

//=======================================================
console.log("Soal No. 3");
console.log("------------------------------------------");

function introduce(name, age, address, hobby) {
  return (
    `Nama Saya ${name}` +
    `, umur saya ${age} tahun` +
    `, alamat saya di ${address}` +
    `, dan saya punya hobby yaitu ${hobby}!`
  );
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
